/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.evolution.observers;

import com.satlib.CSVModel;
import com.satlib.community.CommunityEdge;
import com.satlib.community.CommunityGraph;
import com.satlib.community.CommunityMetric;
import com.satlib.community.CommunityNode;
import com.satlib.community.ConcreteCommunityGraph;
import com.satlib.evolution.EvolutionGraph;
import com.satlib.graph.Node;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author zacknewsham
 */
public class QEvolutionObserver implements CSVEvolutionObserver{
  private final EvolutionGraph graph;
  private final CommunityGraph tmpGraph;
  public final List<CommunityNode> decisions = new ArrayList<>();
  public final HashMap<Integer, Double> qs = new HashMap<>();
  
  public double worstCase = Double.MAX_VALUE;
  public double bestCase = Double.MIN_VALUE;
  private CommunityMetric metric;
  static{
    com.satlib.evolution.observers.CSVEvolutionObserverFactory.getInstance().register("Q", "A graphical representation of the evolution of Q over the solution of the solver (based on the selected community metric)", QEvolutionObserver.class);
  }

  private final CSVModel model = new CSVModel(new String[]{"decision","q"});
  public CSVModel getModel(){
    int i = 0;
    if(model.getRowCount() == 0){
      for(Integer n: qs.keySet()){
        i = (n/10) - 1;
        model.set(i, "decision", n.doubleValue());
        model.set(i, "q", qs.get(n));
      }
    }
    return model;
  }
  
  public QEvolutionObserver(EvolutionGraph graph){
    this.graph = graph;
    tmpGraph = new ConcreteCommunityGraph(){
      @Override
      public Collection<CommunityEdge> getEdges(){
        Collection<CommunityEdge> edges = new ArrayList<>();
        for(CommunityEdge e : super.getEdges()){
          if((e.getStart().getAssignmentState() == null || e.getStart().getAssignmentState() == Node.NodeAssignmentState.UNASSIGNED) &&
                  (e.getEnd().getAssignmentState() == null || e.getEnd().getAssignmentState() == Node.NodeAssignmentState.UNASSIGNED)){
            edges.add(e);
          }
        }
        return edges;
      }
      
      @Override
      public Collection<CommunityNode> getNodes(){
        Collection<CommunityNode> nodes = new ArrayList<>();
        for(CommunityNode n : super.getNodes()){
          if(n.getAssignmentState() == null || n.getAssignmentState() == Node.NodeAssignmentState.UNASSIGNED){
            nodes.add(n);
          }
        }
        return nodes;
      }
      
    };
    for(CommunityNode n : graph.getNodes()){
      tmpGraph.createNode(n.getId(), n.getName());
    }
    for(CommunityEdge e : graph.getEdges()){
     tmpGraph.createEdge(tmpGraph.getNode(e.getStart().getId()), tmpGraph.getNode(e.getEnd().getId()), false);
    }
  }
  
  @Override
  public void setCommunityMetric(CommunityMetric metric){
    this.metric = metric;
  }
  
  @Override
  public void addEdge(CommunityEdge e){
    tmpGraph.createEdge(tmpGraph.getNode(e.getStart().getId()), tmpGraph.getNode(e.getEnd().getId()), false);
  }
  
  @Override
  public void removeEdge(CommunityEdge e){
    CommunityEdge _e = tmpGraph.createEdge(tmpGraph.getNode(e.getStart().getId()), tmpGraph.getNode(e.getEnd().getId()), false);
    tmpGraph.removeEdge(_e);
  }
  public  int windowSize = 10;
  @Override
  public void nodeAssigned(CommunityNode n, Node.NodeAssignmentState state, boolean isDecision) {
    CommunityNode np = tmpGraph.getNode(n.getId());
    np.setAssignmentState(state);
    if(isDecision){
      decisions.add(n);
      if(decisions.size() % windowSize == 0 && decisions.size() != 1){
        if(tmpGraph.getNodes().size() > 1){
          double Q = metric.getCommunities(tmpGraph);
          if(Q < worstCase){
            worstCase = Q;
          }
          if(Q > bestCase){
            bestCase = Q;
          }
          qs.put(decisions.size(), Q);
        }
      }
    }
  }

  @Override
  public void newFileReady() {
    
  }

  @Override
  public String getName() {
    return "Q";
  }

  @Override
  public void updateGraph() {
    
  }
  
}
