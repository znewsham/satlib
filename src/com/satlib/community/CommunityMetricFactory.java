/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.community;

import com.satlib.GenericFactory;
import java.util.HashMap;

/**
 *
 * @author zacknewsham
 */
public class CommunityMetricFactory extends GenericFactory<CommunityMetric>{
  private static final CommunityMetricFactory singleton = new CommunityMetricFactory();
  
  public static CommunityMetricFactory getInstance(){
    return singleton;
  }
  private CommunityMetricFactory(){}
}
