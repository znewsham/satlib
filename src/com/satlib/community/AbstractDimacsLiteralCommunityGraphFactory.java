/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.community;

import com.satlib.graph.Clause;
import com.satlib.graph.DimacsGraphFactory;
import com.satlib.graph.DimacsLiteralGraphFactory;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 *
 * @author zacknewsham
 */
public abstract class AbstractDimacsLiteralCommunityGraphFactory<T extends CommunityGraph> extends DimacsLiteralGraphFactory<T, CommunityNode, CommunityEdge, Clause> implements AbstractCommunityGraphFactory<T>{

  private String metricName;
  protected AbstractDimacsLiteralCommunityGraphFactory(String metricName, HashMap<String, String> patterns) {
    super(patterns);
    this.metricName = metricName;
  }
  
  public HashMap<String, Pattern> getPatterns(){
    return patterns;
  }
  
  @Override
  public T makeGraph(File input) throws FileNotFoundException, IOException{
    if(graph == null){
      this.graph = createGraph();
    }
    super.makeGraph(input);
    
    return graph;
  }
  
  @Override
  public T makeGraph(URL input) throws FileNotFoundException, IOException{
    this.graph = createGraph();
    URLConnection con = input.openConnection();
    this.length = con.getContentLengthLong();
    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
    this.graph = super.makeGraph(in);
    String[] nameParts = input.getFile().split("/");
    graph.setName(nameParts[nameParts.length - 1]);
    return graph;
  }
  
  public T getGraph(){
    return graph;
  }
  public CommunityMetric getMetric(){
    return CommunityMetricFactory.getInstance().getByName(metricName);
  }
}
