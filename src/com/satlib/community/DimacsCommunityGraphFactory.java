/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.community;

import com.satlib.graph.GraphFactoryFactory;
import java.util.HashMap;

/**
 *
 * @author zacknewsham
 */
public class DimacsCommunityGraphFactory extends AbstractDimacsCommunityGraphFactory<CommunityGraph> implements CommunityGraphFactory{
  
  static{
    GraphFactoryFactory.getInstance().register("auto", "cnf", "Load a DIMACS format file into a VIG and compute communities", DimacsCommunityGraphFactory.class);
  }

  public DimacsCommunityGraphFactory(String metricName, HashMap<String, String> patterns) {
    super(metricName, patterns);
  }

  @Override
  public CommunityGraph createGraph() {
    return new ConcreteCommunityGraph() ;
  }
  
}
