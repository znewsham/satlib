/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.community;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author zacknewsham
 */
public class LouvianCommunityMetric implements CommunityMetric{
  static{
    CommunityMetricFactory.getInstance().register("l", "The Louvian community detection algorithm", LouvianCommunityMetric.class);
  }
	private static final long serialVersionUID = 1;
    private CommunityGraph graph;
    private LouvianNetwork network;
    public LouvianCommunityMetric(){
      
    }
    
    public double getCommunities(CommunityGraph graph){
      HashMap<Integer, Integer> newOld = new HashMap<>();
      HashMap<Integer, Integer> oldNew = new HashMap<>();
      ArrayList<CommunityNode> nodes = new ArrayList<>();
      nodes.addAll(graph.getNodes());
      Collections.sort(nodes, new Comparator<CommunityNode>(){
        @Override
        public int compare(CommunityNode t, CommunityNode t1) {
          return t.getId() - t1.getId();
        }
      });

      int currentIndex = 1;
      for(CommunityNode n : nodes){
        oldNew.put(n.getId(), currentIndex);
        newOld.put(currentIndex, n.getId());
        currentIndex++;
      }
      network = new LouvianNetwork(graph, oldNew);
      network.initSingletonClusters();
      network.runLouvainAlgorithm((1 / network.getTotalEdgeWeight()));
      double Q =  network.calcQualityFunction((1 / network.getTotalEdgeWeight()));
      int[][] coms = network.getNodesPerCluster();
      int z = 1;
      int count = 0;
      Community c = null;
      for(int i = 0; i < coms.length; i++){
        if(coms[i].length != 0){
          c = graph.createNewCommunity(z);
        }
        for(int a = 0; a < coms[i].length; a++){
          int index = newOld.get(coms[i][a] + 1);
          CommunityNode n = graph.getNode(index);
          n.setCommunity(z);
          c.addCommunityNode(n);
          count++;
        }
        if(coms[i].length != 0){
          z++;
        }
      }
      if(count != nodes.size()){
        System.err.println("Louvian community detection cannot work with disjoint graphs. Try OL or CNM");
        System.exit(-1);
      }
      graph.setQ(Q);
      return Q;
    }
}
