/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.evolution.observers;

import com.satlib.GenericFactory;
import com.satlib.evolution.EvolutionGraph;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author zacknewsham
 */
public class CSVEvolutionObserverFactory extends GenericFactory<CSVEvolutionObserver>{
  protected static CSVEvolutionObserverFactory singleton = new CSVEvolutionObserverFactory();
  public static CSVEvolutionObserverFactory getInstance(){
    return singleton;
  }
  
  public CSVEvolutionObserver getByName(String name, EvolutionGraph graph){
    CSVEvolutionObserver ret= super.getByName(name, graph);
    if(ret != null){
      EvolutionObserverFactory.getInstance().addObserver(ret);
    }
    return ret;
  }
  
  protected CSVEvolutionObserverFactory(){}
}
