/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib;

/**
 *
 * @author zacknewsham
 */
public interface Progressive {
  public String getProgressionName();
  public double getProgress();
}
