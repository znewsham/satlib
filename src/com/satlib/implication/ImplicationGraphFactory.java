/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.implication;

import com.satlib.graph.GraphFactory;

/**
 *
 * @author zacknewsham
 */
public interface ImplicationGraphFactory extends GraphFactory<ImplicationGraph>{
  
}
