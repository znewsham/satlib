/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.community;

import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TObjectCharHashMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import com.satlib.graph.AbstractGraph;
import com.satlib.graph.Clause;
import com.satlib.graph.Node;
/**
 *
 * @author zacknewsham
 */
public class ConcreteCommunityGraph extends AbstractGraph<CommunityNode, CommunityEdge, Clause> implements CommunityGraph{
  protected final TIntObjectHashMap<Community> communities = new TIntObjectHashMap<Community>();
  private ArrayList<CommunityEdge> dummyEdges = new ArrayList<CommunityEdge>();
  private int edge_count;
  protected TIntIntHashMap varDist = new TIntIntHashMap();
  private double Q;
  public Community getCommunity(int community){
    return communities.get(community);
  }
  
  public int getCommunitySize(int community){
    return communities.get(community).getSize();
  }
  
  public Collection<Community> getCommunities() {
	  return communities.valueCollection();
  }
  
  public int getNumberCommunities(){
	  return communities.size();
  }
  
  public Community createNewCommunity(int communityId) {
    Community com = communities.get(communityId);
    if(com == null){
	  com = new Community(communityId);
	  communities.put(communityId, com);
    }
    return com;
  }
  
  public void removeEdge(CommunityEdge e){
    dummyEdges.remove(e);
    connections.remove(e);
  }
          
  public Iterator<CommunityEdge> getDummyEdges(){
    return dummyEdges.iterator();
  }
  
  @Override
  public CommunityEdge createEdge(CommunityNode a, CommunityNode b, boolean dummy){
    CommunityEdge e = a.getEdge(b);

    if(e != null){
      return e;
    }
    e = new CommunityEdge(a, b, dummy);
    e.id = ++edge_count;
    connections.add(e);
    if(dummy){
      dummyEdges.add(e);
    }
    return e;
  }
  
  @Override
  public CommunityNode createNode(int id, String name, boolean is_head, boolean is_tail){
    synchronized(nodes){
        varDist.put(id, varDist.get(id) + 1);
      if(nodes.containsKey(id)){
        CommunityNode node = nodes.get(id);
        if(name != null){
          node.setName(name);
        }
        uf.add(node);
        return node;
      }
      else{
        CommunityNode n = new CommunityNode(id, name, is_head, is_tail);
        nodes.put(id, n);
        uf.add(n);
        return n;
      }
    }
  }
  

  public int getLargestCommunity(){
    int largest = 0;
    getCommunity(0);
    Iterator<Community> comms = communities.valueCollection().iterator();
    while(comms.hasNext()){
      int size = comms.next().getSize();
      if(size > largest){
        largest = size;
      }
    }
    return largest;
  }

  @Override
  public Clause createClause(TObjectCharHashMap<CommunityNode> nodes) {
    Clause c = new Clause(nodes);
    Clause c1 = clauses.get(c);
    if(c1 != null){
      return c1;
    }
    else{
      if(longestClause == null || longestClause.size() < c.size()){
        longestClause = c;
      }
      clauses.put(c,c);
      return c;
    }
  }
  
  
  @Override
  public void setVariableDistribution(TIntIntHashMap dist){
      this.varDist = dist;
  }
  
  @Override
  public TIntIntHashMap getVariableDistribution(){
      return this.varDist;
  }
  
  public int getMaxLiteral(){
      int max = 0;
      for(Node n : getNodes()){
          if(n.getId() > max){
              max = n.getId();
          }
      }
      return max;
  }
  
  @Override
  public CommunityGraph to3CNF(){
      ConcreteCommunityGraph graph = new ConcreteCommunityGraph();
      int maxLit = getMaxLiteral();
      Iterator<Clause> clauses = getClauses().iterator();
      int found = 0;
      while(clauses.hasNext()){
        Clause c = clauses.next();
        Iterator<Node> nodes = c.getNodes();
        TObjectCharHashMap<CommunityNode> newNodes = new TObjectCharHashMap<>();
          if(c.size() > 3){
            int varCount = 0;
            int limit = 2;
            int processed = 0;
            while(nodes.hasNext()){
                Node n = nodes.next();
                if(limit == 1){
                    newNodes.put(graph.createNode(maxLit, ""), '0');
                }
                newNodes.put(graph.createNode(n.getId(), n.getName()), c.getValue(n) ? '1' : '0');
                varCount++;
                processed++;
                if(varCount == limit){
                    if(processed != c.size() - 1){
                        newNodes.put(graph.createNode(++maxLit, ""), '1');
                    }
                    else{
                        Node n1 = nodes.next();
                        newNodes.put(graph.createNode(n1.getId(), ""), c.getValue(n1) ? '1' : '0');
                    }
                    graph.createEdge((CommunityNode)newNodes.keys()[0], (CommunityNode)newNodes.keys()[1], false);
                    graph.createEdge((CommunityNode)newNodes.keys()[1], (CommunityNode)newNodes.keys()[2], false);
                    graph.createEdge((CommunityNode)newNodes.keys()[0], (CommunityNode)newNodes.keys()[2], false);
                    Clause c1 = graph.createClause(newNodes);
                    newNodes = new TObjectCharHashMap<>();
                    limit = 1;
                    varCount = 0;
                }
            }
          }
          else{
            while(nodes.hasNext()){
                Node n = nodes.next();
                newNodes.put(graph.createNode(n.getId(), n.getName()), c.getValue(n) ? '1' : '0');
            }
            if(newNodes.size() == 2){
                graph.createEdge((CommunityNode)newNodes.keys()[0], (CommunityNode)newNodes.keys()[1], false);
            }
            Clause c1 = graph.createClause(newNodes);
          }
      }
      
      return graph;
  }
  
  
  public int getMinCommunitySize(){
    int smallest = Integer.MAX_VALUE;
    for(Community c : communities.valueCollection()){
      if(c.getSize() < smallest){
        smallest = c.getSize();
      }
    }
    return smallest;
  }
  public int getMaxCommunitySize(){
    return getLargestCommunity();
  }
  public double getMeanCommunitySize(){
    return nodes.size() / (double)communities.size();
  }
  public double getSDCommunitySize(){
    double mean = getMeanCommunitySize();
    double sd = 0;
    for(Community c : communities.valueCollection()){
      sd += Math.pow(mean - c.getSize(), 2);
    }
    
    return Math.sqrt(sd / (double)communities.size());
  }
  
  public int getMinInterEdges(){
    int smallest = Integer.MAX_VALUE;
    for(Community c : communities.valueCollection()){
      if(c.getInterCommunityEdges().size() < smallest){
        smallest = c.getInterCommunityEdges().size();
      }
    }
    return smallest;
  }
  public int getMaxInterEdges(){
    int largest = 0;
    for(Community c : communities.valueCollection()){
      if(c.getInterCommunityEdges().size() > largest){
        largest = c.getInterCommunityEdges().size();
      }
    }
    return largest;
  }
  public double getMeanInterEdges(){
    int total = 0;
    for(Community c : communities.valueCollection()){
      total += c.getInterCommunityEdges().size();
    }
    return (double)total / (double)communities.size();
  }
  public double getSDInterEdges(){
    double mean = getMeanInterEdges();
    double sd = 0;
    for(Community c : communities.valueCollection()){
      sd += Math.pow(mean - c.getInterCommunityEdges().size(), 2);
    }
    
    return Math.sqrt(sd / (double)communities.size());
  }
  
  public int getMinIntraEdges(){
    int smallest = Integer.MAX_VALUE;
    for(Community c : communities.valueCollection()){
      if(c.getIntraCommunityEdges().size() < smallest){
        smallest = c.getIntraCommunityEdges().size();
      }
    }
    return smallest;
  }
  public int getMaxIntraEdges(){
    int largest = 0;
    for(Community c : communities.valueCollection()){
      if(c.getIntraCommunityEdges().size() > largest){
        largest = c.getIntraCommunityEdges().size();
      }
    }
    return largest;
  }
  public double getMeanIntraEdges(){
    int total = 0;
    for(Community c : communities.valueCollection()){
      total += c.getIntraCommunityEdges().size();
    }
    return (double)total / (double)communities.size();
  }
  public double getSDIntraEdges(){
    double mean = getMeanIntraEdges();
    double sd = 0;
    for(Community c : communities.valueCollection()){
      sd += Math.pow(mean - c.getIntraCommunityEdges().size(), 2);
    }
    
    return Math.sqrt(sd / (double)communities.size());
  }
  
  public double getMinEdgeRatio(){
    double smallest = Double.MAX_VALUE;
    for(Community c : communities.valueCollection()){
      double size = (double)c.getInterCommunityEdges().size() / (double)c.getIntraCommunityEdges().size();
      if(size < smallest && !Double.isInfinite(size)){
        smallest = size;
      }
      else if(Double.isInfinite(size)){
        System.err.println("Adjusting for Community with infinte edge ratio");
      }
    }
    return smallest;
  }
  public double getMaxEdgeRatio(){
    double largest = 0;
    for(Community c : communities.valueCollection()){
      double size = (double)c.getInterCommunityEdges().size() / (double)c.getIntraCommunityEdges().size();
      if(size > largest && !Double.isInfinite(size)){
        largest = size;
      }
      else if(Double.isInfinite(size)){
        System.err.println("Adjusting for Community with infinte edge ratio");
      }
    }
    return largest;
  }
  public double getMeanEdgeRatio(){
    double largest = 0;
    int count = communities.size();
    for(Community c : communities.valueCollection()){
      double size = (double)c.getInterCommunityEdges().size() / (double)c.getIntraCommunityEdges().size();
      if(!Double.isInfinite(size) && !Double.isNaN(size)){
        largest += size;
      }
      else{
        System.err.println("Adjusting for Community with infinte edge ratio");
        count--;
      }
    }
    return largest / count;
  }
  public double getSDEdgeRatio(){
    double mean = getMeanEdgeRatio();
    int count = communities.size();
    double sd = 0;
    for(Community c : communities.valueCollection()){
      double size = (double)c.getInterCommunityEdges().size() / (double)c.getIntraCommunityEdges().size();
      if(!Double.isInfinite(size) && !Double.isNaN(size)){
        sd += Math.pow(mean - size, 2);
      }
      else{
        System.err.println("Adjusting for Community with infinte edge ratio");
        count--;
      }
    }
    
    return Math.sqrt(sd / (double)communities.size());
  }
  
  public double getEdgeRatio(){
    int inter = 0;
    int intra = 0;
    for(Community c : communities.valueCollection()){
      inter += c.getInterCommunityEdges().size();
      intra += c.getIntraCommunityEdges().size();
    }
    return (double)inter/(double)intra;
  }
  
  public void removeClause(Clause c){
    clauses.remove(c);
  }

  @Override
  public double getQ() {
    return Q;
  }

  @Override
  public void setQ(double Q) {
    this.Q = Q;
  }
}
