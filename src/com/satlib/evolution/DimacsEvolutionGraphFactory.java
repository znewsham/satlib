/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.evolution;

import com.satlib.NamedFifo;
import com.satlib.community.AbstractDimacsCommunityGraphFactory;
import com.satlib.community.CommunityNode;
import com.satlib.graph.GraphFactoryFactory;
import gnu.trove.map.hash.TIntObjectHashMap;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author zacknewsham
 */
public class DimacsEvolutionGraphFactory extends AbstractDimacsCommunityGraphFactory<EvolutionGraph> implements EvolutionGraphFactory{
  private Evolution evolution;
  protected File dumpFile;
  protected int dumpFreq = 5;
  protected String solver;
  protected File input;
  protected PrintWriter writer = null;
  private File file = null;
  private Process solverProcess;

  int fileNumber = 0;
  int lineNumber = 0;
  
  static{
    GraphFactoryFactory.getInstance().register("auto", "cnf", "Load a DIMACS format file into a VIG and compute communities, then solve and display evolution", DimacsEvolutionGraphFactory.class);
  }
  
  public EvolutionGraph createGraph(){
    return new ConcreteEvolutionGraph();
  }
  public void setSolver(String solver){
    this.solver = solver;
  }
  public String getSolver(){
    return this.solver;
  }
  
  public Evolution getEvolution(){
    return evolution;
  }
  
  public DimacsEvolutionGraphFactory(String metricName, HashMap<String, String> patterns){
    super(metricName, patterns);
  }
  
  public EvolutionGraph makeGraph(File file) throws IOException{
    this.input = file;
    this.graph = new ConcreteEvolutionGraph();
    EvolutionGraph graph = (EvolutionGraph)super.makeGraph(file);
    return graph;
  }
  
}
