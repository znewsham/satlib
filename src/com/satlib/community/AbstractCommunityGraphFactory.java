/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.community;

import com.satlib.graph.GraphFactory;

/**
 *
 * @author zacknewsham
 */
public interface AbstractCommunityGraphFactory<T extends CommunityGraph> extends GraphFactory<T>{
  public CommunityMetric getMetric();
}
