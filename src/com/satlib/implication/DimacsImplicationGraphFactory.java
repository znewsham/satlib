/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.implication;

import com.satlib.graph.DimacsGraphFactory;
import com.satlib.graph.Edge;
import com.satlib.graph.GraphFactoryFactory;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

/**
 *
 * @author zacknewsham
 */
public class DimacsImplicationGraphFactory extends DimacsGraphFactory<ImplicationGraph, ImplicationNode, Edge, ImplicationClause> implements ImplicationGraphFactory{
  static{
    GraphFactoryFactory.getInstance().register("auto", "cnf", "Load a DIMACS format file into a VIG allow manual variable setting", DimacsImplicationGraphFactory.class);
  }
  protected DimacsImplicationGraphFactory(HashMap<String, String> patterns) {
    super(patterns);
  }
  @Override
  public ImplicationGraph getGraph() {
    return graph;
  }
  
  @Override
  public ImplicationGraph makeGraph(URL input) throws IOException{
    return null;
  }
  
  @Override
  public ImplicationGraph makeGraph(File f) throws IOException{
    graph = createGraph();
    super.makeGraph(f);
    Iterator<ImplicationClause> cs = graph.getClauses().iterator();
    this.progress = 0;
    int count = 0;
    while(cs.hasNext()){
      ImplicationClause c = cs.next();
      if(c.size() == 1){
        ImplicationNode n = c.getNodes().next();
        if(c.satisfiedBy(n, true)){
          n.setValue(n.getValue(), ImplicationNode.SET.CONSTANT);
        }
        else{
          n.setValue(!n.getValue(), ImplicationNode.SET.CONSTANT);
        }
      }
      progress = (1.0 / (double)graph.getClausesCount() * ++count);
    }
    return getGraph();
  }

  @Override
  public ImplicationGraph createGraph() {
    return new ConcreteImplicationGraph();
  }
  
}
