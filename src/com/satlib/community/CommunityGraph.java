/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.community;

import gnu.trove.map.hash.TIntIntHashMap;
import java.util.Collection;
import java.util.Iterator;

import com.satlib.graph.Clause;
import com.satlib.graph.Graph;

/**
 *
 * @author zacknewsham
 */
public interface CommunityGraph extends Graph<CommunityNode, CommunityEdge, Clause>{
  
  public double getQ();
  public void setQ(double Q);
  public int getMinCommunitySize();
  public int getMaxCommunitySize();
  public double getMeanCommunitySize();
  public double getSDCommunitySize();
  
  public int getMinInterEdges();
  public int getMaxInterEdges();
  public double getMeanInterEdges();
  public double getSDInterEdges();
  
  public int getMinIntraEdges();
  public int getMaxIntraEdges();
  public double getMeanIntraEdges();
  public double getSDIntraEdges();
  
  public double getMinEdgeRatio();
  public double getMaxEdgeRatio();
  public double getMeanEdgeRatio();
  public double getSDEdgeRatio();
  
  public double getEdgeRatio();
  
  public Community getCommunity(int community);
  
  public int getCommunitySize(int community);
  
  public Collection<Community> getCommunities();
  
  public int getNumberCommunities();
  
  public Community createNewCommunity(int communityId);
 
  public int getLargestCommunity();
  
  public void setVariableDistribution(TIntIntHashMap dist);
  
  public TIntIntHashMap getVariableDistribution();
  
  public CommunityGraph to3CNF();
}
