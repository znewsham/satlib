/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.evolution.observers;

import com.satlib.CSVModel;
import com.satlib.community.CommunityEdge;
import com.satlib.community.CommunityMetric;
import com.satlib.community.CommunityNode;
import com.satlib.evolution.EvolutionGraph;
import com.satlib.graph.Node;
import java.util.Comparator;
import java.util.HashMap;

/**
 *
 * @author zacknewsham
 */
public class VSIDSSpacialLocalityEvolutionObserver implements CSVEvolutionObserver{
  private final EvolutionGraph graph;
  public final HashMap<Integer, Integer> total = new HashMap<>();
  public final HashMap<Integer, Double> ratio = new HashMap<>();
  private Comparator currentComparator = COMMUNITY_COMPARATOR;
  private CommunityMetric metric;
  public int decisions = 0;
  static{
    CSVEvolutionObserverFactory.getInstance().register("VSIDSS", "A graphical representation of the spacial locailty of the VSIDS decision heuristic", VSIDSSpacialLocalityEvolutionObserver.class);
  }
  
  
  private final CSVModel model = new CSVModel(new String[]{"community","decisions","ratio"});
  public CSVModel getModel(){
    
    int i = 0;
    for(Integer n : total.keySet()){
      model.set(i, "community",n.doubleValue());
      model.set(i, "decisions",total.get(n).doubleValue());
      model.set(i, "ratio", ratio.get(n));
      i = i + 1;
    }
    
    return model;
  }
  public VSIDSSpacialLocalityEvolutionObserver(EvolutionGraph graph){
    this.graph = graph;
  }
  
  
  @Override
  public void setCommunityMetric(CommunityMetric metric){
    this.metric = metric;
  }
  
  
  @Override
  public void addEdge(CommunityEdge e){
    
  }
  
  @Override
  public void removeEdge(CommunityEdge e){
    
  }

  @Override
  public void nodeAssigned(CommunityNode n, Node.NodeAssignmentState state, boolean isDecision) {
    if(state == Node.NodeAssignmentState.UNASSIGNED){
      return;
    }
    if(isDecision){
      if(!total.containsKey(n.getCommunity())){
        total.put(n.getCommunity(), 0);
        ratio.put(n.getCommunity(), 0.0);
      }
      decisions++;
      total.put(n.getCommunity(), total.get(n.getCommunity()) + 1);
      ratio.put(n.getCommunity(), (double)total.get(n.getCommunity()) / (double)graph.getCommunitySize(n.getCommunity()));
    }
  }
  
  
  @Override
  public String getName(){
    return "VSIDSS";
  }
  
  @Override
  public void newFileReady() {
    
  }

  @Override
  public void updateGraph() {
    
  }
  
  private static final class SortableDatasetEntry{
    Number value;
    Comparable key;
    public SortableDatasetEntry(Number value, Comparable key){
      this.value = value;
      this.key = key;
    }
  }
  
  private final static Comparator<SortableDatasetEntry> COMMUNITY_COMPARATOR = new Comparator<SortableDatasetEntry>(){
    @Override
    public int compare(SortableDatasetEntry t, SortableDatasetEntry t1) {
      return Integer.parseInt(t.key.toString()) - (Integer.parseInt(t1.key.toString()));
    }
  };
    
  
  private final static Comparator<SortableDatasetEntry> DISTRIBUTION_COMPARATOR = new Comparator<SortableDatasetEntry>(){
    @Override
    public int compare(SortableDatasetEntry t, SortableDatasetEntry t1) {
      return new Double((t.value.doubleValue() - t1.value.doubleValue()) * 10000).intValue();
    }
  };
  
}
