/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.evolution;

import com.satlib.community.AbstractDimacsLiteralCommunityGraphFactory;
import com.satlib.community.CommunityNode;
import com.satlib.graph.GraphFactoryFactory;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author zacknewsham
 */
public class DimacsLiteralEvolutionGraphFactory extends AbstractDimacsLiteralCommunityGraphFactory<EvolutionGraph> implements EvolutionGraphFactory{
  protected File dumpFile;
  protected int dumpFreq = 5;
  protected File input;
  
  static{
    GraphFactoryFactory.getInstance().register("literal", "cnf", "Load a DIMACS format file into a LIG and compute communities, then solve and display evolution", DimacsLiteralEvolutionGraphFactory.class);
  }
  
  public EvolutionGraph createGraph(){
    return new ConcreteEvolutionLiteralGraph();
  }
  
  public DimacsLiteralEvolutionGraphFactory(String metricName, HashMap<String, String> patterns){
    super(metricName, patterns);
  }
  
  public EvolutionGraph makeGraph(File file) throws IOException{
    this.input = file;
    this.graph = new ConcreteLiteralEvolutionGraph();
    EvolutionGraph graph = (EvolutionGraph)super.makeGraph(file);
    Set<CommunityNode> bad = new HashSet<>();
    for(CommunityNode n : graph.getNodes()){
      if(graph.getNode(0 - n.getId()) == null){
        bad.add(n);
      }
    }
    for(CommunityNode n : bad){
      graph.createNode(0 - n.getId(), n.getName());
    }
    return graph;
  }
  
}
