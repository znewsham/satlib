/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.graph;

import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TObjectCharHashMap;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

/**
 *
 * @author zacknewsham
 */
public abstract class DimacsGraphFactory<T1 extends Graph<T2, T3, T4> , T2 extends Node, T3 extends Edge, T4 extends Clause> implements GraphFactory<T1>{
  private Runtime runtime = Runtime.getRuntime();
  private static final int MB = 1024*1024;
  private static String s_all = "All";
  private static String s_named = "Named";
  protected T1 graph;
  private long done = 0;
  protected double progress = 0.0;
  protected long length = 0;
  protected HashMap<String, TIntObjectHashMap<String>> node_lists = new HashMap<>();
  protected HashMap<String, Pattern> patterns = new HashMap<>();
  protected final TIntObjectHashMap<String> all_names = new TIntObjectHashMap<String>();
  
  public HashMap<String, Pattern> getPatterns(){
    return patterns;
  }
  
  public abstract T1 createGraph();
  
  public String getProgressionName(){
    return "Building Graph";
  }
  
  public double getProgress(){
    return (double)done/(double)length;
  }
  
  public DimacsGraphFactory(HashMap<String, String> patterns){
    this.patterns.put(s_all, Pattern.compile(".*"));
    node_lists.put(s_all,new TIntObjectHashMap<String>());
    for(String pattern : patterns.keySet()){
      this.patterns.put(pattern, Pattern.compile(patterns.get(pattern)));
      this.node_lists.put(pattern, new TIntObjectHashMap<String>());
    }
  }
  
  
  
  public HashMap<String, TIntObjectHashMap<String>> getNodeLists(){
    return node_lists;
  }
  
  public T1 makeGraph(BufferedReader reader) throws IOException{
    String line;
    int linecount = 0;
    while((line = reader.readLine()) != null){
      done += line.length() + 1;
      linecount++;
      if(line.length() == 0){
        continue;
      }
      if(line.charAt(0) == '$'){
        continue;
      }
      if(line.charAt(0) == 'c'){
        String[] vars = line.split(" ");
        if(vars.length == 3){
          int id = 0;
          try{
            id = Integer.parseInt(vars[1]);
          }
          catch(NumberFormatException e){
            try{
              id = Integer.parseInt(vars[1].replace("$", ""));
            }
            catch(NumberFormatException e1){
              
            }
          }
          if(id != 0){
            
            Iterator<String> ps = patterns.keySet().iterator();
            while(ps.hasNext()){
              String next = ps.next();
              if(patterns.get(next).matcher(vars[2]).matches()){
                synchronized(node_lists){
                  node_lists.get(next).put(id, vars[2]);
                }
                Node n = graph.createNode(id, vars[2]);
                if(n != null){
                  n.addGroup(next);
                  n.setName(vars[2]);
                }
              }
            }
          }
        }
        continue;
      }
      if(line.charAt(0) == 'p'){
        continue;
      }
      String[] vars = line.split(" ");
      TObjectCharHashMap<T2> nodes = new TObjectCharHashMap<>();
      for(int i = 0; i < vars.length; i++){
    	if (vars[i].compareTo("") == 0){
            continue;
    	}
        int lit1 = Integer.parseInt(vars[i]);
        boolean lit_1value = true;
        if(lit1 == 0){
          continue;
        }
        T2 n = null;
        if(lit1 < 0){
          lit1 = 0 - lit1;
          lit_1value = false;
          graph.decWeight();
        }
        else{
          graph.incWeight();
        }
        synchronized(node_lists){
          if(all_names.containsKey(lit1)){
            n = graph.createNode(lit1, (String)all_names.get(lit1));
            n.addGroup(s_named);
          }
          else{
            n = graph.createNode(lit1, null);
          }
        }
        n.incTimesUsed();
        if(lit_1value){
          n.incTrueApearances();
        }
        else{
          n.incFalseApearances();
        }
        n.addGroup(s_all);
        nodes.put(n, lit_1value ? '1' : '0');
        for(int a = i + 1; a < vars.length; a++){
          if(a == i || vars[a].compareTo("") == 0){
            continue;
          }
          int lit2 = Integer.parseInt(vars[a]);
          boolean lit_2value = true;
          if(lit2 == 0){
            continue;
          }
          if(lit2 < 0){
            lit2 = 0 - lit2;
            lit_2value = false;
          }
          if(lit1 == lit2){
            continue;
          }
          T2 n1 = null;
          if(all_names.containsKey(lit2)){
              n1 = graph.getNode(lit2);
              if(n1 == null){
                n1 = graph.createNode(lit2, (String)all_names.get(lit2));
              }
            n1.addGroup(s_named);
          }
          else{
              n1 = graph.getNode(lit2);
              if(n1 == null){
                n1 = graph.createNode(lit2, null);
              }
          }
          
          nodes.put(n1, lit_2value ? '1' : '0');
          n1.addGroup(s_all);
          if(n != n1){
            graph.connect(n, n1, false);
          }
        }
      }
      if(graph.getClausesCount() % 100 == 0){
        //System.err.printf("%d\n", graph.getClausesCount());
        if(runtime.freeMemory() / MB < 1){
          System.err.println("Out of Memory");
          return null;
        }
      }
      graph.createClause(nodes);
    }
    return graph;
  }
  
  @Override
  public T1 makeGraph(File input) throws FileNotFoundException, IOException{
    BufferedReader reader;
    reader = new BufferedReader(new FileReader(input));
    length = input.length();
    T1 g = makeGraph(reader);
    g.setName(input.getName());
    return g;
  }
  
}
