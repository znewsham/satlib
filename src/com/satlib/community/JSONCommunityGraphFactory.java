/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.community;

import com.satlib.graph.GraphFactoryFactory;
import gnu.trove.map.hash.TIntObjectHashMap;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author zacknewsham
 */
public class JSONCommunityGraphFactory implements CommunityGraphFactory{
  private JSONCommunityGraph graph;
  protected HashMap<String, Pattern> patterns = new HashMap<>();
  private static String s_all = "All";
  private static String s_named = "Named";
  private long length = 0;
  private double progress = 0.0;
  protected HashMap<String, TIntObjectHashMap<String>> node_lists = new HashMap<>();
  
  public HashMap<String, Pattern> getPatterns(){
    return patterns;
  }
  static{
    GraphFactoryFactory.getInstance().register("auto", "sb", "Load a graph representation from SB (JSON) file", JSONCommunityGraphFactory.class);
  }
  
  protected JSONCommunityGraphFactory(String ignore, HashMap<String, String> patterns) {
    this.patterns.put(s_all, Pattern.compile(".*"));
    node_lists.put(s_all,new TIntObjectHashMap<String>());
    for(String pattern : patterns.keySet()){
      this.patterns.put(pattern, Pattern.compile(patterns.get(pattern)));
      this.node_lists.put(pattern, new TIntObjectHashMap<String>());
    }
  }
  @Override
  public CommunityMetric getMetric() {
    return new CommunityMetric() {
      @Override
      public double getCommunities(CommunityGraph graph) {
        //TODO: add getGraph().blahblah
        return 0.0;
      }
    };
  }
  
  private CommunityGraph makeGraph(JSONObject json){
    return graph = new JSONCommunityGraph((JSONObject)json.get("graphViewer"));
  }
  
  private CommunityGraph makeGraph(BufferedReader reader){
    StringBuilder contents = new StringBuilder();
    String line;
    try {
      while((line = reader.readLine()) != null){
        contents.append(line).append("\n");
        progress = (double)length/(double)contents.length();
      }
    } catch (IOException ex) {
      Logger.getLogger(JSONCommunityGraphFactory.class.getName()).log(Level.SEVERE, null, ex);
    }
    JSONObject json = (JSONObject)JSONValue.parse(contents.toString());
    return makeGraph(json);
  }

  @Override
  public CommunityGraph makeGraph(File input) throws FileNotFoundException, IOException {
    this.length = input.length();
    BufferedReader reader = new BufferedReader(new FileReader(input));
    return makeGraph(reader);
  }

  @Override
  public CommunityGraph makeGraph(URL input) throws FileNotFoundException, IOException {
    URLConnection con = input.openConnection();
    this.length = con.getContentLengthLong();
    BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
    return makeGraph(reader);
  }

  @Override
  public CommunityGraph getGraph() {
    return graph;
  }

  @Override
  public HashMap<String, TIntObjectHashMap<String>> getNodeLists() {
    return node_lists;
  }

  @Override
  public String getProgressionName() {
    return  "Building graph from JSON";
  }

  @Override
  public double getProgress() {
    return progress;
  }
  
}
