/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.evolution.observers;

import com.satlib.community.CommunityEdge;
import com.satlib.community.CommunityMetric;
import com.satlib.community.CommunityNode;
import com.satlib.graph.Clause;
import com.satlib.graph.Node;

/**
 *
 * @author zacknewsham
 */
public interface EvolutionObserver {
  void addEdge(CommunityEdge e);
  void removeEdge(CommunityEdge e);
  void nodeAssigned(CommunityNode n, Node.NodeAssignmentState state, boolean isDecision);
  void newFileReady();
  String getName();
  void setCommunityMetric(CommunityMetric metric);
  void updateGraph();
}
