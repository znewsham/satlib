/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib;

import com.satlib.community.CommunityMetric;
import java.util.HashMap;

/**
 *
 * @author zacknewsham
 */
public class SimpleFactory <T extends Class> {
  private final HashMap<String, T> classes = new HashMap<>();
    
  public void register(String name, T c){
    classes.put(name, c);
  }
  
  public <T> T getByName(String name){
    if(classes.get(name) == null){
      return null;
    }
    else{
      try {
        return (T)classes.get(name).newInstance();
      } catch (InstantiationException | IllegalAccessException ex) {
        return null;
      }
    }
  }
  
}
