/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.evolution;

/**
 *
 * @author zacknewsham
 */
public interface EvolutionGraphFactoryObserver {
  public enum Action{
    process,
    newline,
    addgraph
    
  }
  void notifyObserver(EvolutionGraphFactory factory, Action action);
  String getName();
}

