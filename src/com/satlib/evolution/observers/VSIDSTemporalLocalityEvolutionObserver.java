/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.evolution.observers;

import com.satlib.CSVModel;
import com.satlib.community.CommunityEdge;
import com.satlib.community.CommunityMetric;
import com.satlib.community.CommunityNode;
import com.satlib.evolution.EvolutionGraph;
import com.satlib.graph.Node;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author zacknewsham
 */
public class VSIDSTemporalLocalityEvolutionObserver implements CSVEvolutionObserver{
  private final EvolutionGraph graph;
  private static final int VARS_PER_REDRAW = 10;
  private final List<CommunityNode> decisions = new ArrayList<>();
  public final HashMap<CommunityNode, List<CommunityNode>> propogations = new HashMap<>();
  public final HashMap<Integer, Integer> decisionComs = new HashMap<>();
  public final HashMap<Integer, Integer> propogationComs = new HashMap<>();
  private CommunityMetric metric;
  public int worstCase;
  static{
    CSVEvolutionObserverFactory.getInstance().register("VSIDST", "A graphical representation of the temporal locailty of the VSIDS decision heuristic", VSIDSTemporalLocalityEvolutionObserver.class);
  }
  
  
  private final CSVModel model = new CSVModel(new String[]{"decision","communitycount_d","communitycount_p"});
  public CSVModel getModel(){
    int i = 0;
    for(Integer n : decisionComs.keySet()){
      int pcount = 0;
      if(propogationComs.containsKey(n)){
        pcount = propogationComs.get(n);
      }
      model.set(i, "decision",n.doubleValue());
      model.set(i, "communitycount_d",decisionComs.get(n).doubleValue());
      model.set(i, "communitycount_p",(double)pcount);
      i = i + 1;
    }
    return model;
  }
  public VSIDSTemporalLocalityEvolutionObserver(EvolutionGraph graph){
    this.graph = graph;
    propogations.put(null, new ArrayList<CommunityNode>());
  }

  @Override
  public void addEdge(CommunityEdge e){
    
  }
  
  @Override
  public void removeEdge(CommunityEdge e){
    
  }
  
  @Override
  public void setCommunityMetric(CommunityMetric metric){
    this.metric = metric;
  }
  
  @Override
  public synchronized void nodeAssigned(CommunityNode n, Node.NodeAssignmentState state, boolean isDecision) {
    if(state == Node.NodeAssignmentState.UNASSIGNED){
      return;
    }
    int windowSize = 10;
    if(isDecision){
      decisions.add(n);
      propogations.put(n, new ArrayList<CommunityNode>());
      if(decisions.size() == 1){
        Set<Integer> propogationComs = new HashSet<>();
        for(CommunityNode p : propogations.get(null)){
          propogationComs.add(p.getCommunity());
        }
      }
    }
    else{
      CommunityNode d = null;
      if(!decisions.isEmpty()){
        d = decisions.get(decisions.size() - 1);
      }
      propogations.get(d).add(n);
    }
    if(isDecision && !decisions.isEmpty() && (decisions.size() % windowSize) == 0){
      Set<Integer> decisionComs = new HashSet<>();
      Set<Integer> propogationComs = new HashSet<>();
      for(int i = decisions.size() - 10; i < decisions.size(); i++){
        decisionComs.add(decisions.get(i).getCommunity());
        if(i > 1){
          CommunityNode d = decisions.get(i - 1);
          for(CommunityNode p : propogations.get(d)){
            propogationComs.add(p.getCommunity());
          }
        }
      }
      this.decisionComs.put(decisions.size(), decisionComs.size());
      this.propogationComs.put(decisions.size(), propogationComs.size());
      if(decisionComs.size() > worstCase){
        worstCase = decisionComs.size();
      }
      decisionComs.clear();
      propogationComs.clear();
    }
  }
  

  @Override
  public void newFileReady() {
    
  }
  
  public String getName(){
    return "VSIDST";
  }

  @Override
  public void updateGraph() {
  }

  
}
