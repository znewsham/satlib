/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.community;

import com.satlib.graph.GraphFactoryFactory;
import java.util.HashMap;

/**
 *
 * @author zacknewsham
 */
public class DimacsLiteralCommunityGraphFactory extends AbstractDimacsLiteralCommunityGraphFactory<CommunityGraph> implements CommunityGraphFactory{
  
  static{
    GraphFactoryFactory.getInstance().register("literal", "cnf", "Load a DIMACS format file into a LIG and compute communities", DimacsLiteralCommunityGraphFactory.class);
  }

  public DimacsLiteralCommunityGraphFactory(String metricName, HashMap<String, String> patterns) {
    super(metricName, patterns);
  }

  @Override
  public CommunityGraph createGraph() {
    return new ConcreteCommunityGraph() ;
  }
  
}
