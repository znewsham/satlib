/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.graph;

import gnu.trove.map.hash.TObjectCharHashMap;
import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

/**
 *
 * @author zacknewsham
 * @param <T>
 * @param <T1>
 * @param <T2>
 */
public interface Graph <T extends Node, T1 extends Edge, T2 extends Clause>{
  public String getName();
  public void setName(String name);
  public int getTotalEdges();
  public int getWeight();
  public void incWeight();
  public void decWeight();
  public int getMaxNodeId();
  public T2 getLongestClause();
  public T1 getEdge(T a, T b);
  
  public T1 createEdge(T a, T b, boolean dummy);
  
  public T2 createClause(TObjectCharHashMap<T> nodes);
  
  public T1 connect(T a, T b, boolean dummy);
  
  public void union(T a, T b);
  
  public boolean connected(T a, T b);
  
  public T getNode(int id);
  
  public void removeNode(Node n);
  
  public T createNode(int id, String name);
  
  public T createNode(int id, String name, boolean head, boolean tail);
  
  public Collection<T> getNodes(String set);
  
  public Collection<T> getNodes();
  
  public int getNodeCount();
  
  public Collection<T2> getClauses();
  
  public int getClausesCount();

  public Collection<T1> getEdges();
  
  public int indexOf(T node);

  public void writeDimacs(File dimacsFile) throws IOException;
  
  public Graph<T, T1, T2> to3CNF();
  
  public void removeClause(Clause c);
  
  
  public Iterator<T1> getDummyEdges();
  
  public void removeEdge(T1 e);
}
