package com.satlib;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import com.satlib.graph.Graph;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

/**
 *
 * @author zacknewsham
 */
public abstract class GenericFactory <E>{
  private final HashMap<String, Class<? extends E>> classes = new HashMap<>();
  private final HashMap<String, String> descriptions = new HashMap<>();
  
  public void register(String name, String description, Class<? extends E> c){
    classes.put(name, c);
    descriptions.put(name, description);
  }
  
  public String[] getDescriptions(){
    String[] names = new String[classes.size()];
    descriptions.values().toArray(names);
    return names;
  }
  
  public String[] getNames(){
    String[] names = new String[classes.size()];
    classes.keySet().toArray(names);
    return names;
  }
  
  public E getByName(String name, Graph graph){
    if(classes.get(name) == null){
      return null;
    }
    else{
      try {
        Constructor<? extends E> con = classes.get(name).getConstructor(Graph.class);
        return con.newInstance(graph);
      } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | IllegalArgumentException | InvocationTargetException | SecurityException ex) {
        ex.printStackTrace();
        ex.getCause().printStackTrace();
        return null;
      }
    }
  }
  
  
  public E getByName(String name){
    if(classes.get(name) == null){
      return null;
    }
    else{
      try {
        return classes.get(name).newInstance();
      } catch (InstantiationException | IllegalAccessException ex) {
        return null;
      }
    }
  }
  
  protected GenericFactory(){}
}
