/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.evolution;

import com.satlib.community.CommunityNode;
import com.satlib.community.ConcreteCommunityGraph;
import com.satlib.graph.Node;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author zacknewsham
 */
public class ConcreteEvolutionGraph extends ConcreteCommunityGraph implements EvolutionGraph{
	private Node decisionVariable = null;
    private List<CommunityNode> decisions = new ArrayList<>();
	private boolean showDecisionVariable = true;
	private int displayDecisionVariableFor = 100;
	private int evolutionSpeed = 10;
	public void setDecisionVariable(Node n) {
	    this.decisionVariable = n;
	}

	public Node getDecisionVariable() {
	    return this.decisionVariable;
	}

	public void clearDecisionVariable() {
	    this.decisionVariable = null;
	}
	
	public void setShowDecisionVariable(boolean showDecisionVariable) {
		this.showDecisionVariable = showDecisionVariable;
	}
	
	public boolean getShowDecisionVariable() {
		return this.showDecisionVariable;
	}
	
	public int getDisplayDecisionVariableFor() {
		return this.displayDecisionVariableFor;
	}
	
	public void setDisplayDecisionVariableFor(int length) {
		this.displayDecisionVariableFor = length;
	}
	
	public void setEvolutionSpeed(int speed) {
		this.evolutionSpeed = speed;
	}
	
	public int getEvolutionSpeed() {
		return this.evolutionSpeed;
	}
    
    public void recordDecisionVariable(CommunityNode n){
      decisions.add(n);
    }
    public Collection getDecisionVariables(){
      return decisions;
    }
}
