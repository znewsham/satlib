/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.evolution;

import com.satlib.community.CommunityGraph;
import com.satlib.community.CommunityNode;
import com.satlib.graph.Node;
import java.util.Collection;

/**
 *
 * @author zacknewsham
 */
public interface EvolutionGraph extends CommunityGraph{
  void setDecisionVariable(Node n);
  Node getDecisionVariable();
  void clearDecisionVariable();
  void setShowDecisionVariable(boolean showDecisionVariable);
  boolean getShowDecisionVariable();
  int getDisplayDecisionVariableFor();
  void setDisplayDecisionVariableFor(int length);
  void setEvolutionSpeed(int speed);
  int getEvolutionSpeed();
  void recordDecisionVariable(CommunityNode n);
  Collection getDecisionVariables();
}
