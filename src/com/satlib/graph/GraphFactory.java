/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.graph;

import com.satlib.Progressive;
import gnu.trove.map.hash.TIntObjectHashMap;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 *
 * @author zacknewsham
 */
public interface GraphFactory<T extends Graph> extends Progressive{
  public T makeGraph(File input) throws FileNotFoundException, IOException;
  public T makeGraph(URL input) throws FileNotFoundException, IOException;
  public T getGraph();
  public HashMap<String, TIntObjectHashMap<String>> getNodeLists();
  public HashMap<String, Pattern> getPatterns();
}
