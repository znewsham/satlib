
package com.satlib.evolution;

import com.satlib.NamedFifo;
import com.satlib.community.CommunityEdge;
import com.satlib.community.CommunityGraph;
import com.satlib.community.CommunityNode;
import com.satlib.evolution.observers.EvolutionObserver;
import com.satlib.evolution.observers.CSVEvolutionObserverFactory;
import com.satlib.evolution.observers.EvolutionObserverFactory;
import com.satlib.graph.Edge;
import com.satlib.graph.LiteralGraph;
import com.satlib.graph.Node;
import gnu.trove.map.hash.TIntObjectHashMap;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author zacknewsham
 */
public class Evolution {
  
  public static int maxLinesPerFile = 1000000;
  
  public static String dumpFileDirectory = System.getProperty("user.dir") + "/solvers/piping/";
  public static String pipeFileName = dumpFileDirectory + "myPipe.txt";
  public static String outputDirectory = dumpFileDirectory + "output/";
  protected String metricName;
  
  private final EvolutionGraph graph;
  static int linesPerFile = 1000000;
  private int totalFiles = 0;
  private int currentFile = -1;
  private int nextFile = -1;
  private int currentPosition = -1;
  private int totalLines = 0;
  private int currentConflict = 0;
  private int desiredConflict = 0;
  private boolean isScanningForConflict = false;
  private final List<Node> updatedNodes = new ArrayList<>();
  private final List<Edge> updatedEdges = new ArrayList<>();
  private List<String> currentFileLines = null;
  private List<String> nextFileLines = null;
  private Thread bufferThread = null;
  private int lastDecisionVariable = -1;
  private int displayDecisionVariableCount = 0;
  private boolean updateInProgress = false;

  private int nextLineAdvancingTo = 0;
  private Thread evolutionThread = null;
  private final File input;
  private final String solver;
	  
  public Evolution(EvolutionGraph graph, File input, String solver){
    this.graph = graph;
    this.input = input;
    this.solver = solver;
  }
  
  public String getSolver(){
    return solver;
  }
  public File getFile(){
    return input;
  }
  int decisions = 0;
  public int getDecisions(){
    return decisions;
  }
  
  public void updatePosition(int position, boolean timerTriggered) {
    if (getCurrentPosition() != position && !(currentPosition == -1 && position == 0)) {
      advanceEvolution(getCurrentPosition(), position, timerTriggered);
    }
  }
  public boolean scanToConflict(int conflictNumber, boolean timerTriggered) {

    if ((conflictNumber > getCurrentConflict() && getCurrentPosition() >= getTotalLines() - 1) || (conflictNumber < getCurrentConflict() && getCurrentPosition() <= 0)) {
      return false;
    }

    this.isScanningForConflict = true;
    this.desiredConflict = conflictNumber;

    if (conflictNumber > getCurrentConflict()) {
      updatePosition(getTotalLines(), timerTriggered);
    } else if (conflictNumber < getCurrentConflict()) {
      updatePosition(0, timerTriggered);
    }

    return true;
  }

  private void parseLine(String line, boolean forwards, int lineNumber, boolean timerTriggered) {
    if (line.charAt(0) == 'v') {
      parseNodeLine(line, forwards, lineNumber, timerTriggered);
    } else if (line.charAt(0) == 'c') {
      parseEdgeLine(line, forwards);
    } else if (line.charAt(0) == '!') {
      foundConflict(line);
    }
  }

  private void foundConflict(String line) {
    this.setCurrentConflict(Integer.parseInt(line.split(" ")[1]));
  }

  private void parseNodeLine(String line, boolean forwards, int lineNumber, boolean timerTriggered) {
    CommunityGraph graph = this.graph;
    Node.NodeAssignmentState state = Node.NodeAssignmentState.UNASSIGNED;
    CommunityNode n = null;
    int activity = 0;
    boolean stateFound = false;
    boolean isDecisionVariable = false;
    boolean activityFound = false;
    Set<CommunityNode> nodes = new HashSet<>();
    for (String c : line.split(" ")) {
      if (c.compareTo("v") == 0) { // Start of line
        continue;
      } else if (c.compareTo("d") == 0) {
        if (forwards) {
          isDecisionVariable = true;
        }
        continue;
      } else if (c.compareTo("p") == 0) {
        // This is just a propagation variable. Do nothing with it at the moment.
        continue;
      } else if (!stateFound) { // Var state
        stateFound = true;

        if (forwards) { // Not necessary to do if in reverse
          switch (Integer.parseInt(c)) {
            case 0:
              state = Node.NodeAssignmentState.ASSIGNED_FALSE;
              break;
            case 1:
              state = Node.NodeAssignmentState.ASSIGNED_TRUE;
              break;
            default:
              state = Node.NodeAssignmentState.UNASSIGNED;
          }
        }
      } 
      else if (!activityFound) {
        activity = Integer.parseInt(c);
        activityFound = true;
      } 
      else if(!(graph instanceof LiteralGraph)){
        n = graph.getNode(Math.abs(Integer.parseInt(c)));
        nodes.add(n);
      }
      else{
        n = graph.getNode(Integer.parseInt(c));
        nodes.add(n);
        n = graph.getNode(0 - Integer.parseInt(c));
        nodes.add(n);
      }
    }
    if(isDecisionVariable){
      decisions++;
    }
    for(CommunityNode _n : nodes){
      n = _n;
      if (n != null) {
        Node.NodeAssignmentState prevState = n.getAssignmentState();
        n.setActivity(activity);
        if (isDecisionVariable) {
          this.graph.recordDecisionVariable(n);
        }
        for (EvolutionObserver observer : EvolutionObserverFactory.getInstance().observers()) {
          observer.nodeAssigned(n, state, isDecisionVariable);
        }
        if (isDecisionVariable && this.graph.getShowDecisionVariable() && getLastDecisionVariable() != lineNumber) {
          this.graph.setDecisionVariable(n);
          lastDecisionVariable = lineNumber;

          if (timerTriggered) {
            return;
          }
        }

        if (forwards && state != prevState) {
          n.setAssignmentState(state);
        } else if (!forwards) {
          n.revertToPreviousAssignmentState();
        }

        if (n.getAssignmentState() != prevState || isDecisionVariable) // Will redraw the node if it has changed at all
        {
          getUpdatedNodes().add(n);
        }
        if (n.getAssignmentState() != prevState) {
          getUpdatedEdges().addAll(n.getEdges());
        }
      }
    }
  }
  
  boolean hasError = false;
  public boolean hasError(){
    return hasError;
  }

  private void parseEdgeLine(String line, boolean forwards) {
    ArrayList<CommunityNode> nodes = new ArrayList<CommunityNode>();
    CommunityGraph graph = this.graph;
    boolean addEdge = true;

    for (String c : line.split(" ")) {
      if (c.compareTo("c") == 0) { // Start of line
        continue;
      } else if (c.compareTo("0") == 0) { // End of line
        break;
      } else if (c.compareTo("+") == 0) {
        addEdge = true;
      } else if (c.compareTo("-") == 0) {
        addEdge = false;
      } else if(graph instanceof LiteralGraph){
        nodes.add(graph.getNode(Integer.parseInt(c)));
      }
      else{
        nodes.add(graph.getNode(Math.abs(Integer.parseInt(c))));
      }
    }

    CommunityNode n1;
    CommunityNode n2;
    CommunityEdge e;
    Edge.EdgeState prevState;
    if(nodes.size() > 500){
      System.err.printf("Clause too long (%d vars)\n", nodes.size());
      hasError = true;
      return;
    }
    else if(nodes.size() > 200){
      System.err.printf("Long clause (%d vars)...\n", nodes.size());
    }
    for (int i = 0; i < nodes.size() - 1; i++) {
      n1 = nodes.get(i);
      for (int j = i + 1; j < nodes.size(); j++) {
        n2 = nodes.get(j);
        e = graph.getEdge(n1, n2);

        if (e == null) {
          e = graph.connect(n1, n2, false);

          if (addEdge) {
            e.setEdgeAsConflict();
          }
        }
        for (EvolutionObserver observer : EvolutionObserverFactory.getInstance().observers()) {
          if(addEdge){
            observer.addEdge(e);
          }
          else{
            observer.removeEdge(e);
          }
        }

        prevState = e.getAssignmentState();

        if (forwards) {
          if (addEdge) {
            e.setAssignmentState(Edge.EdgeState.SHOW);
          } else {
            e.setAssignmentState(Edge.EdgeState.HIDE);
          }
        } else {
          e.revertToPreviousAssignmentState();
        }

        if (e.getAssignmentState() != prevState) {
          getUpdatedEdges().add(e);
        }
      }
    }
  }

  public void newFileReady(int numLinesInFile) {
    System.err.println("new file ready");
    setTotalFiles(getTotalFiles() + 1);
    setTotalLines(getTotalLines() + numLinesInFile);
    for(EvolutionObserver observer : EvolutionObserverFactory.getInstance().observers()){
      observer.newFileReady();
    }

    if (nextFileLines == null && getBufferThread() == null) {
      bufferFile(getTotalFiles() - 1);
    }
  }

  public void bufferFile(final int fileNumber) {
    setNextFile(fileNumber);
    Runnable r = new Runnable() {
      @Override
      public void run() {
        try {
          nextFileLines = Files.readAllLines(Paths.get(outputDirectory + Integer.toString(fileNumber) + ".txt"), Charset.defaultCharset());
        } 
        catch (IOException e) {
          System.out.println("Error opening output file.");
        }
      }
    };
    bufferThread = new Thread(r);
    getBufferThread().start();
  }

  private void updateBufferedLines(int lineInCurrentFile, boolean forwards) {
    /*if(getTotalFiles() == 1 && getCurrentFile() == 0){
      //dont mess with it
    }
    else if (getCurrentFile() >= getTotalFiles() - 1 && forwards) {
      forwards = false;
    } 
    else if (getCurrentFile() <= 0 && !forwards) {
      forwards = true;
    }*/

    boolean closeToEndOfCurrentFile = forwards && (lineInCurrentFile >= (int) (0.9 * linesPerFile));
    boolean closeToStartOfCurrentFile = !forwards && (lineInCurrentFile <= (int) (0.1 * linesPerFile));

    boolean updateBuffer = nextFileLines == null || closeToEndOfCurrentFile || closeToStartOfCurrentFile;

    if (updateBuffer) {
      int nf = -1;

      if (forwards && getNextFile() != getCurrentFile() + 1) {
        nf = getCurrentFile() + 1;
      } else if (!forwards && getNextFile() != getCurrentFile() - 1) {
        nf = getCurrentFile() - 1;
      }

      if (nf != -1) {
        bufferFile(nf);
      }
    }
  }

  private String getLine(int lineNumber, boolean forwards) throws InterruptedException {
    if(lineNumber >= getTotalLines()){
      return "";
    }
    int fileOfLine = getFileNumberFromLine(lineNumber);

    boolean isInCurrentFile = fileOfLine == getCurrentFile();
    boolean isInNextFile = fileOfLine == getNextFile();

    if (!isInCurrentFile && !isInNextFile) {
      while(getBufferThread() == null){
        Thread.sleep(1000);
      }
      if (getBufferThread().isAlive()) {
        getBufferThread().join();
      }

      nextFileLines = null;
      setNextFile(-1);
      bufferFile(fileOfLine);
      getBufferThread().join();
      isInNextFile = true;
    }

    if (isInNextFile) {
      if (getBufferThread().isAlive()) {
        getBufferThread().join();
      }

      currentFileLines = nextFileLines;
      setCurrentFile(getNextFile());
      nextFileLines = null;
      setNextFile(-1);
    }

    int lineInCurrentFile = adjustOverallLineToCurrentFileLine(lineNumber);
    updateBufferedLines(lineInCurrentFile, forwards);
    // Must have the proper lines in the buffer
    return getCurrentFileLines().get(lineInCurrentFile);
  }

  public void advanceEvolution(int startlingLine, int endingLine, final boolean _timerTriggered) {
    this.nextLineAdvancingTo = endingLine;

    if (this.getEvolutionThread() == null || !this.isUpdateInProgress()) {
      this.setUpdateInProgress(true);
      graph.clearDecisionVariable();

      Runnable r = new Runnable() {

        @Override
        public void run() {
          boolean repeat = true;
          boolean timerTriggered = _timerTriggered;
          while (repeat) {
            int el = getNextLineAdvancingTo();
            repeat = false;

            try {
              advanceEvolutionThread(getCurrentPosition(), el, timerTriggered);
            } catch (InterruptedException e) {
              System.out.println("Error advancing the evolution.");
            } finally {
              repeat = el != getNextLineAdvancingTo();
            }
          }

          if (graph.getDecisionVariable() == null || !_timerTriggered) {
            setUpdateInProgress(false);
          }
        }
      };

      this.evolutionThread = new Thread(r);
      this.getEvolutionThread().start();
    }
  }

  private void updateGraph() {
    for(EvolutionObserver observer : EvolutionObserverFactory.getInstance().observers()){
      observer.updateGraph();
    }
    getUpdatedEdges().clear();
    getUpdatedNodes().clear();
  }

  public void advanceEvolutionThread(int startingLine, int endingLine, boolean timerTriggered) throws InterruptedException {
    int lastLine = endingLine;

    if (startingLine < endingLine) {
      lastLine = forwardsEvolution(startingLine, endingLine, timerTriggered);
    } else {
      lastLine = backwardsEvolution(startingLine, endingLine, timerTriggered);
    }

    setCurrentPosition(lastLine);
    updateGraph();
  }

  private int forwardsEvolution(int startingLine, int endingLine, boolean timerTriggered) throws InterruptedException {
    for (int i = startingLine + 1; i <= Math.min(endingLine, getTotalLines() - 1); i++) {
      parseLine(getLine(i, true), true, i, timerTriggered);

      if (this.isScanningForConflict()) {
        if (this.getCurrentConflict() >= this.getDesiredConflict()) {
          this.isScanningForConflict = false;
          graph.clearDecisionVariable();
          return i;
        }
      } 
      else if (graph.getDecisionVariable() != null && timerTriggered) {
        return i - 1;
      }
    }

    return endingLine;
  }

  private int backwardsEvolution(int startingLine, int endingLine, boolean timerTriggered) throws InterruptedException {
    for (int i = Math.min(startingLine, getTotalLines() - 1); i > endingLine; i--) {
      parseLine(getLine(i, false), false, i, timerTriggered);

      if (this.isScanningForConflict()) {
        if (this.getCurrentConflict() <= this.getDesiredConflict()) {
          this.isScanningForConflict = false;
          graph.clearDecisionVariable();
          return i;
        }
      }
    }

    if (endingLine == 0) {
      this.setCurrentConflict(0);
    }

    return endingLine;
  }

  private int adjustOverallLineToCurrentFileLine(int lineNumber) {
    return lineNumber % linesPerFile;
  }

  private int getFileNumberFromLine(int lineNumber) {
    int fileNumber = 0;
    while (lineNumber > linesPerFile - 1) {
      fileNumber++;
      lineNumber -= linesPerFile;
    }

    return fileNumber;
  }

  /**
   * @return the totalFiles
   */
  public int getTotalFiles() {
    return totalFiles;
  }

  /**
   * @param totalFiles the totalFiles to set
   */
  public void setTotalFiles(int totalFiles) {
    this.totalFiles = totalFiles;
  }

  /**
   * @return the currentFile
   */
  public int getCurrentFile() {
    return currentFile;
  }

  /**
   * @param currentFile the currentFile to set
   */
  public void setCurrentFile(int currentFile) {
    this.currentFile = currentFile;
  }

  /**
   * @return the nextFile
   */
  public int getNextFile() {
    return nextFile;
  }

  /**
   * @param nextFile the nextFile to set
   */
  public void setNextFile(int nextFile) {
    this.nextFile = nextFile;
  }

  /**
   * @return the currentPosition
   */
  public int getCurrentPosition() {
    return currentPosition;
  }

  /**
   * @param currentPosition the currentPosition to set
   */
  public void setCurrentPosition(int currentPosition) {
    this.currentPosition = currentPosition;
  }

  /**
   * @return the totalLines
   */
  public int getTotalLines() {
    return totalLines;
  }

  /**
   * @param totalLines the totalLines to set
   */
  public void setTotalLines(int totalLines) {
    this.totalLines = totalLines;
  }

  /**
   * @return the currentConflict
   */
  public int getCurrentConflict() {
    return currentConflict;
  }

  /**
   * @param currentConflict the currentConflict to set
   */
  public void setCurrentConflict(int currentConflict) {
    this.currentConflict = currentConflict;
  }

  /**
   * @return the desiredConflict
   */
  public int getDesiredConflict() {
    return desiredConflict;
  }

  /**
   * @return the isScanningForConflict
   */
  public boolean isScanningForConflict() {
    return isScanningForConflict;
  }

  /**
   * @return the updatedNodes
   */
  public List<Node> getUpdatedNodes() {
    return updatedNodes;
  }

  /**
   * @return the updatedEdges
   */
  public List<Edge> getUpdatedEdges() {
    return updatedEdges;
  }

  /**
   * @return the currentFileLines
   */
  public List<String> getCurrentFileLines() {
    return currentFileLines;
  }

  /**
   * @return the bufferThread
   */
  public Thread getBufferThread() {
    return bufferThread;
  }

  /**
   * @return the lastDecisionVariable
   */
  public int getLastDecisionVariable() {
    return lastDecisionVariable;
  }

  /**
   * @return the displayDecisionVariableCount
   */
  public int getDisplayDecisionVariableCount() {
    return displayDecisionVariableCount;
  }

  /**
   * @return the nextLineAdvancingTo
   */
  public int getNextLineAdvancingTo() {
    return nextLineAdvancingTo;
  }

  /**
   * @return the evolutionThread
   */
  public Thread getEvolutionThread() {
    return evolutionThread;
  }

  public void setDisplayDecisionVariableCount(int displayDecisionVariableCount) {
    this.displayDecisionVariableCount = displayDecisionVariableCount;
  }

  /**
   * @return the updateInProgress
   */
  public boolean isUpdateInProgress() {
    return updateInProgress;
  }

  /**
   * @param updateInProgress the updateInProgress to set
   */
  public void setUpdateInProgress(boolean updateInProgress) {
    this.updateInProgress = updateInProgress;
  }
  
  
  
  protected PrintWriter writer = null;
  private Process solverProcess;
  private File file = null;

  int fileNumber = 0;
  int lineNumber = 0;
  
  public void buildEvolutionFile() {
    Runnable r = new Runnable() {

      @Override
      public void run() {
        try {

          NamedFifo fifo = new NamedFifo(Evolution.pipeFileName);
          
          if (fifo.getFile().exists()){
            fifo.getFile().delete();
          }
    	  fifo.create();
          Thread.sleep(1000);
          solverProcess = Runtime.getRuntime().exec(String.format(getSolver().concat(" -pipe=%s %s"), Evolution.pipeFileName, input.getAbsolutePath()));
          isRunning = true;
          Thread t = new Thread(){
            public void run(){
              try {
                solverProcess.waitFor();
              } 
              catch (InterruptedException ex) {
                Logger.getLogger(DimacsEvolutionGraphFactory.class.getName()).log(Level.SEVERE, null, ex);
              }
               isRunning = false;
            }
          };
          t.start();
          Thread.sleep(1000);
          BufferedReader reader = openPipedFile();
          String line;
          int i = 0;
          while ((line = reader.readLine()) != null) {
            outputLine(line);
            i++;
          }

          closeWriter();
          reader.close();
          fifo.getFile().delete();
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    };
    Thread t = new Thread(r);
    t.start();
  }
  
  
  protected void closeWriter() {
    newFileReady(getLineNumber());
  }
  
  public void close(){
    if(writer != null){
      writer.close();
    }
  }
  
  private BufferedReader openPipedFile() {
    Timer timer = new Timer();
    TimerTask watchdog = new TimerTask(){
      @Override
      public void run() {
       System.err.println("Unable to open pipe file: " + Evolution.pipeFileName);
      }
    };
    timer.schedule(watchdog, 30000);
    
	try {
      return new BufferedReader(new FileReader(Evolution.pipeFileName));
	} 
    catch (Exception e) {
      Logger.getLogger(DimacsEvolutionGraphFactory.class.getName()).log(Level.SEVERE, null, e);
      return openPipedFile();
	}
    finally{
      watchdog.cancel();
    }
  }
  
  protected void outputLine(String line) {
    if (writer == null || lineNumber == Evolution.linesPerFile) {
      if (lineNumber == Evolution.linesPerFile) {
        closeWriter();
      }
      
      file = new File(Evolution.outputDirectory + fileNumber + ".txt");
      if(!file.getParentFile().exists()){
        file.getParentFile().mkdirs();
      }

      try {
        writer = new PrintWriter(file, "UTF-8");
      } catch (Exception e) {
        System.out.println("Unable to create output file. Please make sure that you have the proper permissions.");
      }

      lineNumber = 0;
      fileNumber++;
    }
    
    if (!file.exists()){
      System.out.println("there was a problem creating the file");
      return;
    }
    
    writer.println(line);
    //why is this necessary? (It is)
    writer.flush();
    lineNumber++;
  }

  public void start(){
    createOutputFolder();
    buildEvolutionFile();
  }
  private void createOutputFolder() {
    if(solver != null){
      File dir = new File(Evolution.outputDirectory);

      try {
        deleteOutputFolder();
        dir.mkdir();
      } catch (Exception e) {
        System.out.println("Unabled to create directory in solvers/piping/output. Do you have the correct permissions?");
      }
    }
  }
  
  public void deleteOutputFolder() {
	  File dir = new File(Evolution.outputDirectory);

	  try {
        if (dir.exists()) {
          for (File f : dir.listFiles()) {
            f.delete();
          }

          dir.delete();
        }
      } catch (Exception e) {
        System.out.println("Unabled to delete directory in solvers/piping/output. Do you have the correct permissions?");
      }
  }


  public int getLineNumber(){
    return lineNumber;
  }
  
  
  
  public void stopSolver() {
    solverProcess.destroy();
  }
  

  private boolean isRunning = true;
  public boolean solverRunning(){
    return isRunning;
  }
}
