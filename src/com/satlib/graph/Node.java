/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.graph;

import gnu.trove.map.hash.TIntObjectHashMap;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author zacknewsham
 * @param <T>
 */
public class Node<T extends Edge>{
  public static enum NodeAssignmentState { ASSIGNED_FALSE, ASSIGNED_TRUE, UNASSIGNED }
  public static enum NodeState { SHOW, HIDE }
  
  private int trueAppearances = 0;
  private int falseAppearances = 0;
	
  private final HashSet<String> groups = new HashSet<String>();
  private final TIntObjectHashMap<T> connections = new TIntObjectHashMap();
  private final ArrayList<T> connections2 = new ArrayList<>();
  public static final NameComparator NAME_COMPARATOR =new NameComparator();
  private int id;
  private String name;
  private int timesReused = 0;
  private int set = 0; //1 = decision, 2 = implication
  private NodeAssignmentState assignmentState;
  public static final boolean UNIQUE_EDGES = true;
  private NodeState state;
  private List<NodeAssignmentState> assignmentStateHistory = null;
  private int activity = 0;
  
  public Node(int id, String name){
    this(id, name, false, false);
    size = 0;
  }
  public Node(int id, String name, boolean is_head, boolean is_tail){
    this.id = id;
    this.name = name;
    state = NodeState.SHOW;
    assignmentState = NodeAssignmentState.UNASSIGNED;
    assignmentStateHistory = new ArrayList<NodeAssignmentState>();
    assignmentStateHistory.add(assignmentState);
    /*if(this.name == null){
      return;
    }*/
  size = 0;
  }

  private double size;
  
  public double getSize() {
    return size;
  }
  
  public void setSize(double size) {
    this.size = size;
  }
  public String toJson(){
    StringBuilder json = new StringBuilder();
    json.append("{");
    json.append("\"id\":");
    json.append(this.getId());
    json.append(",\"name\":\"");
    json.append(this.getName());
    json.append("\",\"groups\":[");
    for(String group : groups){
      json.append("{\"group\":\"");
      json.append(group);
      json.append("\"}");
    }
    json.append("]}");
    return json.toString();
  }
  
  public int hashCode(){
    return this.id;
  }
  public boolean equals(Object o1) {
    Node o = (Node) o1;
    return this.id == o.id;
  }
  
  public String getName(){
    return this.name == null ? String.valueOf(id) : this.name;
  }
  
  @Override
  public String toString(){
    return this.name == null ? String.format("noname (%d)", id) : this.name.concat(String.format("(%d)",id));
  }
  
  public Collection<T> getEdges(){
    if(UNIQUE_EDGES){
      return this.connections.valueCollection();
    }
    else{
      return connections2;
    }
  }
  
  public T getEdge(Node n){
    if(UNIQUE_EDGES){
      return connections.get(n.getId());
    }
    else{
      Iterator<T> edges = getEdges().iterator();
      while(edges.hasNext()){
        T next = edges.next();
        if((next.getStart() == this && next.getEnd() == n) || (next.getStart() == n && next.getEnd() == this)){
          return next;
        }
      }
      return null;
    }
  }
  public void addEdge(T e){
    if(UNIQUE_EDGES){
      connections.put(e.getOtherId(id), e);
    }
    else{
      connections2.add(e);
    }
  }
  
  public int getId(){
    return this.id;
  }
  public void addGroup(String group){
    groups.add(group);
  }
  public Collection<String> getGroups(){
    return groups;
  }

  public boolean inGroup(String set) {
    return groups.contains(set);
  }

  public void setName(String name) {
    this.name = name;
  }
  public boolean getValue(){
    return false;
  }
  public boolean isSet(){
    return false;
  }

  
  public void removeEdge(T e){
    if(UNIQUE_EDGES){
      connections.remove(e.getOtherId(id));
    }
    else{
      connections2.remove(e);
    }
  }
  
  
  /*@Override
  public int getX(GraphViewer graph) {
    return graph.getX(this);
  }

  @Override
  public int getY(GraphViewer graph) {
    return graph.getY(this);
  }
  
  public Color getFillColor(GraphViewer graph){
    return graph.getFillColor(this);
  }
  
  public Color getColor(GraphViewer graph){
    return graph.getColor(this);
  }
  */
  
  private static class NameComparator implements Comparator<Node>{
    @Override
    public int compare(Node o1, Node o2) {
      AlphanumComparator comp = new AlphanumComparator();
      return comp.compare(o1.getName(), o2.getName());
    }
  }
  
  public void setState(NodeState state) {
	  this.state = state;
  }
  
  public NodeState getState() {
	  return this.state;
  }
  
  public boolean isVisible() {
	  return this.state == NodeState.SHOW;
  }
  
  public NodeAssignmentState getAssignmentState() {
	  return this.assignmentState;
  }
  
  public boolean isAssigned() {
	  return this.assignmentState != NodeAssignmentState.UNASSIGNED;
  }
  
  public void setAssignmentState(NodeAssignmentState state) {
	  this.assignmentState = state;
	  assignmentStateHistory.add(state);
  }
  
  public void revertToPreviousAssignmentState() {
	  int lastElement = assignmentStateHistory.size()-1;
	  assignmentStateHistory.remove(lastElement);
	  this.assignmentState = assignmentStateHistory.get(Math.min(0, lastElement-1));
  }
  
  public int getTimesUsed(){
    return timesReused;
  }
  
  public void incTrueApearances(){
    trueAppearances++;
  }
  
  public void incFalseApearances(){
    falseAppearances++;
  }
  
  public double getTrueRatio(){
    return 1.0/(double)timesReused*(double)trueAppearances;
  }
  
  public void incTimesUsed(){
    timesReused ++;
  }
  public void setActivity(int activity) {
    this.activity = activity;
  }
  
  public int getActivity() {
    return this.activity;
  }
}
