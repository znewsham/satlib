/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satlib.evolution.observers;

import com.satlib.CSVModel;

/**
 *
 * @author zacknewsham
 */
public interface CSVEvolutionObserver extends EvolutionObserver{
  CSVModel getModel();
}
