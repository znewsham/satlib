/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.graph;

import com.satlib.community.DimacsCommunityGraphFactory;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author zacknewsham
 */

public class GraphFactoryFactory {
  private static final GraphFactoryFactory singleton = new GraphFactoryFactory();
  private final HashMap<String, HashMap<String, String>> descriptions = new HashMap<>();
  private final HashMap<String, HashMap<String, Class<? extends GraphFactory>>> classes = new HashMap<>();
  
  public static GraphFactoryFactory getInstance(){
    return singleton;
  }
  
    
  public void register(String name, String extension, String description, Class<? extends GraphFactory> c){
    if(!classes.containsKey(name)){
      classes.put(name, new HashMap<String, Class<? extends GraphFactory>>());
      descriptions.put(name, new HashMap<String, String>());
    }
    classes.get(name).put(extension, c);
    descriptions.get(name).put(extension, description);
  }
  
  public String[] getExtensions(String name){
    return classes.get(name).keySet().toArray(new String[classes.get(name).size()]);
  }
  public String getDescription(String name, String extension){
    return descriptions.get(name).get(extension);
  }
  
  public String[] getNames(){
    String[] names = new String[classes.size()];
    classes.keySet().toArray(names);
    return names;
  }
  
  public GraphFactory getByNameAndExtension(String name, String extension, String metricName, HashMap<String,String> patterns  ){
    if(classes.get(name).get(extension) == null){
      return null;
    }
    else{
      try {
        
        Constructor c = classes.get(name).get(extension).getConstructor(String.class, patterns.getClass());
        return (GraphFactory)c.newInstance(metricName, patterns);
      } catch (InstantiationException | IllegalArgumentException | NoSuchMethodException | SecurityException | IllegalAccessException ex) {
        return null;
      }
      catch(InvocationTargetException ex){
        ex.printStackTrace();
        ex.getCause().printStackTrace();
        return null;
      }
    }
  }
  private GraphFactoryFactory(){}
  
}
