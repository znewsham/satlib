/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.community;

import gnu.trove.map.hash.TIntIntHashMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import gnu.trove.map.hash.TObjectCharHashMap;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import com.satlib.graph.AbstractGraph;
import com.satlib.graph.Clause;
import com.satlib.graph.DrawableNode;
import com.satlib.graph.Node;

/**
 *
 * @author zacknewsham
 */
public class JSONCommunityGraph extends AbstractGraph<CommunityNode, CommunityEdge, Clause> implements CommunityGraph{
  private HashMap<Integer, Point> nodePositions = new HashMap<>();
  private HashMap<Point, CommunityNode> nodesByPosition = new HashMap<>();
  private HashMap<String, TIntObjectHashMap<String>> node_lists = new HashMap<>();
  private HashMap<Integer, Community> communities = new HashMap<>();
  private JSONObject tmp;
  private TIntIntHashMap varDist = new TIntIntHashMap();
  private double progress = 0.0;
  private double Q = 0.0;
  public JSONCommunityGraph(JSONObject json){
    tmp = json;
  }
  public void init(){
    if(tmp == null){
      return;
    }
    //this.Q = (double)tmp.get("Q");
    JSONArray jNodes = (JSONArray)tmp.get("nodes");
    int done = 0;
    for(Object n : jNodes){
      JSONObject jNode = (JSONObject)n;
      CommunityNode node = CommunityNode.fromJson(jNode, this);
      Point p = new Point(((Long)jNode.get("x")).intValue(),((Long)jNode.get("y")).intValue());
      nodesByPosition.put(p, node);
      nodePositions.put(node.getId(), p);
      if(communities.get(node.getCommunity()) == null){
        Community com = new Community(node.getCommunity());
        communities.put(node.getCommunity(), com);
      }
      communities.get(node.getCommunity()).addCommunityNode(node);
      Iterator<String> groups = node.getGroups().iterator();
      while(groups.hasNext()){
        String group = groups.next();
        if(node_lists.get(group) == null){
          node_lists.put(group, new TIntObjectHashMap<String>());
          nodes_set.put(group, new ArrayList<CommunityNode>());
        }
        node_lists.get(group).put(node.getId(), node.getName());
        nodes_set.get(group).add(node);
      }
      nodes.put(node.getId(), node);
      done ++;
      progress = ((double)done/(double)jNodes.size()) / 2.0;
    }
    done = 0;
    JSONArray jEdges = (JSONArray)tmp.get("edges");
    for(Object e : jEdges){
      JSONObject jEdge = (JSONObject)e;
      CommunityEdge edge = CommunityEdge.fromJson(jEdge, this);
      if(edge.getStart().getCommunity() == edge.getEnd().getCommunity()){
        communities.get(edge.getStart().getCommunity()).addIntraCommunityEdge(edge);
      }
      else{
        communities.get(edge.getStart().getCommunity()).addInterCommunityEdge(edge);
        communities.get(edge.getEnd().getCommunity()).addInterCommunityEdge(edge);
      }
      edge.getStart().addEdge(edge);
      edge.getEnd().addEdge(edge);
      connections.add(edge);
      done ++;
      progress = ((double)done/(double)jEdges.size());
    }
    tmp = null;
  }
  
  public HashMap<String, TIntObjectHashMap<String>> getNodeLists(){
    return node_lists;
  }
  
  @Override
  public Community getCommunity(int community) {
    return communities.get(community);
  }

  @Override
  public int getCommunitySize(int community) {
    return communities.get(community).getNodeCount();
  }

  @Override
  public Collection<Community> getCommunities() {
    return communities.values();
  }

  @Override
  public int getNumberCommunities() {
    return communities.size();
  }

  @Override
  public Community createNewCommunity(int communityId) {
    Community c = new Community(communityId);
    communities.put(communityId, c);
    return c;
  }

  @Override
  public int getLargestCommunity() {
    int size = 0;
    int largest = -1;
    for(Community c : communities.values()){
      if(size < c.getSize()){
        size = c.getSize();
        largest = c.getId();
      }
    }
    return largest;
  }

  @Override
  public Iterator<CommunityEdge> getDummyEdges() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public void removeEdge(CommunityEdge e) {
    connections.remove(e);
    e.getStart().removeEdge(e);
    e.getEnd().removeEdge(e);
  }

  @Override
  public void setVariableDistribution(TIntIntHashMap dist) {
    this.varDist = dist;
  }

  @Override
  public TIntIntHashMap getVariableDistribution() {
    return varDist;
  }

  @Override
  public CommunityGraph to3CNF() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public CommunityNode getNode(int id) {
    return nodes.get(id);
  }

  @Override
  public void removeNode(Node n) {
    Point p = nodePositions.get(n.getId());
    nodePositions.remove(n.getId());
    nodesByPosition.remove(p);
    nodes.remove(n.getId());
    for(CommunityEdge e : ((CommunityNode)n).getEdges()){
      removeEdge(e);
    }
  }

  @Override
  public CommunityNode createNode(int id, String name) {
    CommunityNode n = new CommunityNode(id, name);
    nodes.put(n.getId(), n);
    return n;
  }

  @Override
  public CommunityNode createNode(int id, String name, boolean head, boolean tail) {
    CommunityNode n = new CommunityNode(id, name);
    nodes.put(n.getId(), n);
    return n;
  }

  @Override
  public Collection<CommunityNode> getNodesList() {
    return nodes.valueCollection();
  }

  @Override
  public Collection<CommunityNode> getNodes(String set) {
    return nodes_set.get(set);
  }


  @Override
  public Collection<CommunityNode> getNodes() {
    return nodes.valueCollection();
  }

  @Override
  public int getNodeCount() {
    return nodes.size();
  }

  @Override
  public Collection<Clause> getClauses() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public int getClausesCount() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public Collection<CommunityEdge> getEdges() {
    return connections;
  }

  @Override
  public void writeDimacs(File dimacsFile) throws IOException {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public Clause createClause(TObjectCharHashMap nodes) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public CommunityEdge createEdge(CommunityNode a, CommunityNode b, boolean dummy) {
    CommunityEdge e = new CommunityEdge(a, b, dummy);
    connections.add(e);
    a.addEdge(e);
    b.addEdge(e);
    return e;
  }

  public CommunityNode getNodeAtXY(int x, int y, double scale) {
    x /= scale;
    y /= scale;
    for(int i = x - DrawableNode.NODE_DIAMETER; i < x + DrawableNode.NODE_DIAMETER; i++){
      for(int a = y - DrawableNode.NODE_DIAMETER; a < y + DrawableNode.NODE_DIAMETER; a++){
        Point p = new Point(i, a);
        CommunityNode n = nodesByPosition.get(p);
        if(n != null){
          return n;
        }
      }
    }
    return null;
  }

  public int getX(CommunityNode node) {
    return nodePositions.get(node.getId()).x;
  }

  public int getY(CommunityNode node) {
    return nodePositions.get(node.getId()).y;
  }

  @Override
  public int getMinCommunitySize() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public int getMaxCommunitySize() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public double getMeanCommunitySize() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public double getSDCommunitySize() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public int getMinInterEdges() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public int getMaxInterEdges() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public double getMeanInterEdges() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public double getSDInterEdges() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public int getMinIntraEdges() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public int getMaxIntraEdges() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public double getMeanIntraEdges() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public double getSDIntraEdges() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public double getMinEdgeRatio() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public double getMaxEdgeRatio() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public double getMeanEdgeRatio() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public double getSDEdgeRatio() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public double getEdgeRatio() {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }
  
  
  
  public void removeClause(Clause c){
      throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }

  @Override
  public double getQ() {
    return Q;
  }

  @Override
  public void setQ(double Q) {
    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
  }
  
}
