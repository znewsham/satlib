/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.satlib.community;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public class Community extends CommunityGraphAdapter{
  	// Constants
	private final ArrayList<CommunityEdge> intraCommunityEdges = new ArrayList<CommunityEdge>();
	private final ArrayList<CommunityEdge> interCommunityEdges = new ArrayList<CommunityEdge>();
	//private HashMap<Integer, Integer> edgesCount = new HashMap<>();
    protected HashSet<CommunityNode> communityNodes = new HashSet<CommunityNode>();
	private int id;
	
	public Community(int id) {
		this.id = id;
	}
	
    public int getMaxCount(){
      /*Iterator<Integer> ec = edgesCount.values().iterator();
      int max = 0;
      while(ec.hasNext()){
        int i = ec.next();
        if(i > max){
          max = i;
        }
      }
      return max;*/
      return 0;
    }
    private static HashMap<Community, Integer>edgesTo = new HashMap<>();
    public int getInterEdgesTo(Community c){
      if(edgesTo.containsKey(c)){
        return edgesTo.get(c);
      }
      int count = 0;
      for(CommunityEdge e : this.getInterCommunityEdges()){
        if(e.getStart().getCommunity() == this.getId()){
          count += e.getEnd().getCommunity() == c.getId() ? 1 : 0;
        }
        else{
          count += e.getStart().getCommunity() == c.getId() ? 1 : 0;
        }
      }
      edgesTo.put(c, count);
      return count;
    }
    public int indexOf(CommunityNode node){
      Iterator<CommunityNode> nI = communityNodes.iterator();
      int i = 0;
      while(nI.hasNext()){
        CommunityNode n1 = nI.next();
        if(n1 == node){
          return i;
        }
        i++;
      }
      return -1;
    }
	public void addIntraCommunityEdge(CommunityEdge edge) {
		intraCommunityEdges.add(edge);
	}
	
	public void addInterCommunityEdge(CommunityEdge edge) {
		interCommunityEdges.add(edge);
        int c = edge.getStart().getCommunity() == this.getId() ? edge.getEnd().getCommunity() : edge.getStart().getCommunity();
        //int a = edgesCount.get(c);
        //edgesCount.put(c, a += 1);
	}
	
	public void addCommunityNode(CommunityNode node) {
        communityNodes.add(node);
	}
	
	public Collection<CommunityEdge> getIntraCommunityEdges() {
      if(intraCommunityEdges.isEmpty()){
        interCommunityEdges.clear();
        for(CommunityNode node : communityNodes){
          for(CommunityEdge e : node.getEdges()){
            if(e.isInterCommunityEdge()){
              interCommunityEdges.add(e);
            }
            else{
              intraCommunityEdges.add(e);
            }
          }
        }
      }
      return intraCommunityEdges;
	}
	
	public Collection<CommunityEdge> getInterCommunityEdges() {
      if(interCommunityEdges.isEmpty()){
        intraCommunityEdges.clear();
        for(CommunityNode node : communityNodes){
          for(CommunityEdge e : node.getEdges()){
            if(e.isInterCommunityEdge()){
              interCommunityEdges.add(e);
            }
            else{
              intraCommunityEdges.add(e);
            }
          }
        }
      }
		return interCommunityEdges;
	}
	
	public Collection<CommunityNode> getCommunityNodes() {
		return communityNodes;
	}
	
	public int getId() {
		return id;
	}

	public int getSize() {
		return communityNodes.size();
	}

    @Override
    public Community getCommunity(int community) {
      return this;
    }

    @Override
    public int getCommunitySize(int community) {
      return this.getSize();
    }

    @Override
    public Collection<Community> getCommunities() {
      ArrayList<Community> ret = new ArrayList<Community>();;
      ret.add(this);
      return ret;
    }

    @Override
    public int getNumberCommunities() {
      return 1;
    }

    @Override
    public int getLargestCommunity() {
      return 0;
    }

    public Iterator<CommunityEdge> getDummyEdges(){
      return new ArrayList<CommunityEdge>().iterator();
    }
    
    @Override
    public CommunityNode getNode(int id) {
      
      /*List l = new ArrayList(communityNodes);
      Collections.sort(l, new Comparator<CommunityNode>(){

        @Override
        public int compare(CommunityNode o1, CommunityNode o2) {
          
        }
        
      });*/
      Iterator<CommunityNode> nodes = getNodes().iterator();
      while(nodes.hasNext()){
        CommunityNode next = nodes.next();
        if(next.getId() == id){
          return next;
        }
      }
      return null;
    }

    @Override
    public Collection<CommunityNode> getNodesList() {
      return communityNodes;
    }

    @Override
    public Collection<CommunityNode> getNodes(String set) {
      return communityNodes;
    }

    @Override
    public Collection<CommunityNode> getNodes() {
      return communityNodes;
    }

    @Override
    public int getNodeCount() {
      return communityNodes.size();
    }

    @Override
    public Collection<CommunityEdge> getEdges() {
      return getIntraCommunityEdges();
    }


    @Override
    public boolean connected(CommunityNode a, CommunityNode b) {
      return true;
    }
	
}
