/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satlib.evolution.observers;

import com.satlib.GenericFactory;
import com.satlib.evolution.EvolutionGraph;
import com.satlib.evolution.observers.EvolutionObserver;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author zacknewsham
 */
public class EvolutionObserverFactory extends GenericFactory<EvolutionObserver>{
  private static final EvolutionObserverFactory singleton = new EvolutionObserverFactory();
  protected final List<EvolutionObserver> observers = new ArrayList<>();
  private EvolutionObserverFactory(){}
  
  public static EvolutionObserverFactory getInstance(){
    return singleton;
  }
  
  public void addObserver(EvolutionObserver obs){
    observers.add(obs);
  }
  public EvolutionObserver getByName(String name, EvolutionGraph graph){
    EvolutionObserver ret = super.getByName(name, graph);
    if(ret != null){
      observers.add(ret);
    }     
    return ret;
  }
  
  public List<EvolutionObserver> observers(){
    return observers;
  }
}
